#/usr/env sh

# Installing pathogen
mkdir -p ~/.vim/autoload ~/.vim/bundle ~/tmp && \
    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

# Installing plugins
cd ~/.vim/bundle

git clone "https://github.com/scrooloose/nerdtree"
git clone "https://github.com/itchyny/lightline.vim"
git clone "https://github.com/easymotion/vim-easymotion"
git clone "https://github.com/junegunn/fzf"
git clone "https://github.com/junegunn/fzf.vim"
git clone "https://github.com/airblade/vim-gitgutter"
git clone "https://github.com/tpope/vim-fugitive"
git clone "https://github.com/dense-analysis/ale"
git clone "https://github.com/davidhalter/jedi-vim"
git clone "https://github.com/sainnhe/forest-night"

find . -type d -name .git | xargs rm -rf
find . -type d -name .github | xargs rm -rf
rm -rf ale/test
