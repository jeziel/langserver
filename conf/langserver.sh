export PS1='\w> '
# installing ripgrep requires me to install gcc musl-dev
#export FZF_DEFAULT_COMMAND='rg --files'
export FZF_DEFAULT_COMMAND='find * -type f'

# space before command won't get in history.
export HISTCONTROL=ignorespace

#alias tmux="TERM=screen-256color-bce tmux"

source /root/venv/bin/activate
