#/bin/env bash

COMMAND=$1
USER=$(whoami)
IMG_NAME="${USER}-langserver"

case $COMMAND in
    build)
        image_target=$2
        case $image_target in
            base)
                image_name="${IMG_NAME}-base"
                docker build \
                    -f Dockerfile.base \
                    --tag $image_name \
                    --no-cache .
                ;;
            langserver)
                image_name="${IMG_NAME}"
                docker build \
                    -f Dockerfile.langserver \
                    --tag $image_name \
                    --no-cache .
                ;;
            *)
                echo "build an image"
                echo "build base # the base used in the langserver"
                echo "build langserver # the actual langserver"
                exit 1
                ;;
        esac
        ;;
    debug)
        image_name="${IMG_NAME}-base"
        docker run --rm -it --entrypoint sh $image_name
        ;;
    run)
        CODE=$2
        PWD=$(pwd)
        docker run --rm -it --entrypoint sh \
            -v $CODE:/root/Code \
            $IMG_NAME \
            -l # this is for sh
        ;;
    *)
        echo "Usage:"
        echo " operation.sh build  # build a new docker image."
        echo " operation.sh debug  # allows you to run a container."
        echo " operation.sh run ~/code"
        exit 1
        ;;
esac
